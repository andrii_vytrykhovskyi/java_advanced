import exception.DuplicateIdException;
import exception.NotFoundIdException;
import model.Project;
import model.TeamLead;
import model.Worker;
import service.ProjectService;
import service.TeamLeadService;
import service.WorkerService;
import service.impl.ProjectServiceImpl;
import service.impl.TeamLeadServiceImpl;
import service.impl.WorkerServiceImpl;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException, NotFoundIdException, DuplicateIdException {
        ProjectService projectService = new ProjectServiceImpl();
        //System.out.println(projectService.getAll());
        //projectService.getById(1545632434);
        //projectService.create(new Project("a","a",23));
        //projectService.delete(5);
        //projectService.update(1888312689, new Project(1,"fdf","df", 23));

        TeamLeadService teamLeadService = new TeamLeadServiceImpl();
        //teamLeadService.create(new TeamLead(2,"andri", Date.valueOf("1998-01-02"), 1000));
        //System.out.println(teamLeadService.getAll());
        //teamLeadService.getById(5);
        //teamLeadService.delete(2);


        WorkerService workerService = new WorkerServiceImpl();
        //workerService.create(new Worker(3,"sdfsdf","sdfdsf", 12211));
        //System.out.println(workerService.getAll());
        //workerService.getById(2);
        //workerService.delete(6);
        //workerService.update(8, new Worker(4,"sdfsdf","sdfdsf", 12211));
    }
}