$('.message a').click(function () {
    $('form').animate({
        height: "toggle",
        opacity: "toggle"
    }, "slow");
});

$(document).ready(function () {
    $("button#register").click(function () {
        var name = $("form.register-form input.name").val();
        var budget = $("form.register-form input.budget").val();
        var password = $("form.register-form input.password").val();
        var confirmPassword = $("form.register-form input.confirmPassword").val();
        if (name == '' || budget == '' || password == '' || confirmPassword == '') {
            alert("Please fill all fields...!!!!!!");
        } else if ((password.length) < 8) {
            alert("Password should at least have 8 character...!!!!!!");
        } else if (!(password).match(confirmPassword)) {
            alert("Your passwords don't match. Try again!");
        } else {
            var projectRegistration = {
                projectName: name,
                projectBudget: budget,
                projectPassword: password
            };
            $.post("registration", projectRegistration, function (data) {
                if (data === 'Success') {
                    $("form")[0].reset();
                } else if (data == 'To login') {
                    alert("User is already registered");
                }
            });
        }
    });
});

$(document).ready(function () {
    $("button#login").click(function () {
        var projectName = $("form.login-form input.name").val();
        var password = $("form.login-form input.password").val();


        if (projectName == '' || password == '') {
            alert("Please fill all fields...!!!!!!");
        } else {
            var project = {
                name: projectName,
                password: password
            };
            $.post("login", project, function (data) {
                if (data == 'Wrong password') {
                    alert("Wrong password! Try again")
                } else if (data == 'User is NOT registered') {
                    alert("You are NOT registered! Please register yourself.")
                } else if (data != '') {
                    redirection(data);
                }
                $("form")[1].reset();
            });
        }
    });
});

function redirection(data) {
    if (data != '') {
        let finalUrl = '';
        let url = window.location.href.split('/');
        for (let i = 0; i < url.length - 1; i++) {
            finalUrl += url[i] + '/';
        }
        finalUrl += data.destinationUrl;
        window.location.href = finalUrl;
    }
}