$(document).ready(function () {
    $("form input#createWorker").click(function () {
        var name = $("form input#name").val();
        console.log(name);
        var title = $("form input#title").val();
        console.log(title);
        var salary = $("form input#salary").val();
        console.log(salary);
        if (name == '' || title == '' || salary == '') {
            alert("Please fill all fields...!!!!!!");
        } else {
            var worker = {
                workerName: name,
                workerTitle: title,
                workerSalary: salary
            };
            $.post("worker", worker, function (data) {
                if (data === 'Success') {
                    $("form")[0].reset();
                    alert("Worker:" + worker.workerName + " was created")
                }
            });
        }
    });
});