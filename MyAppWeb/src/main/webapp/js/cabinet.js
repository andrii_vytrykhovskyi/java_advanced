let workers = null;

$.get("workers", function (data) {
    if (data != '') {
        workers = data;
    }
}).done(function () {
    let cardsContent = "";

    jQuery.each(workers, function (i, worker) {
        cardsContent += "<div class='card'>" +
            "<div class='card-body'>" +
            "<h5 class='card-title'>Worker name: " + worker.fullName + "</h5>" +
            "<h6 class='card-subtitle mb-2 text-muted'> Salary: " + worker.salary + "</h6>" +
            "<p class='card-text'>Title: " + worker.title + "</p>" +
            "<a href='worker?id=" + worker.id + "'class='card-link'>About worker</a>" +
            "</div>" +
            "</div>"
    });

    $("div#workerCards").html(cardsContent);
});