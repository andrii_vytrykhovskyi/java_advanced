package dao.impl;

import dao.WorkerDAO;
import model.Worker;
import util.SQLConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkerDAOImpl implements WorkerDAO {
    private static final String GET = "select * from worker";
    private static final String GET_ID = "select * from worker where id = ?";
    private static final String INSERT = "insert into worker (id, full_name, title, salary) values (?, ?, ?, ?)";
    private static final String DELETE = "delete from worker where id=?";
    private static final String UPDATE = "update worker set full_name = ?, title = ?, salary = ? where (id = ?)";

    private Connection sqlConnector;

    public WorkerDAOImpl() {
        sqlConnector = SQLConnector.getConnection();
    }

    @Override
    public void update(int id, Worker worker) throws SQLException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(UPDATE)) {
            statement.setString(1, worker.getFullName());
            statement.setString(2, worker.getTitle());
            statement.setDouble(3, worker.getSalary());
            statement.setInt(4, id);
            statement.execute();
        }
    }

    @Override
    public List<Worker> getAll() throws SQLException {
        List<Worker> workers = new ArrayList<>();
        try (Statement statement = sqlConnector.createStatement();
             ResultSet resultSet = statement.executeQuery(GET)) {
            while (resultSet.next()) {
                Worker worker = new Worker(resultSet.getInt("id"), resultSet.getString("full_name"), resultSet.getString("title"), resultSet.getInt("salary"));
                workers.add(worker);
            }
        }
        return workers;
    }

    @Override
    public Worker getById(int id) throws SQLException {
        Worker worker = null;
        try (PreparedStatement statement = sqlConnector.prepareStatement(GET_ID)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    worker = new Worker(resultSet.getInt("id"), resultSet.getString("full_name"), resultSet.getString("title"), resultSet.getInt("salary"));
                }
            }
        }
        return worker;
    }

    @Override
    public void create(Worker worker) throws SQLException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(INSERT)) {
            statement.setInt(1, worker.getId());
            statement.setString(2, worker.getFullName());
            statement.setString(3, worker.getTitle());
            statement.setInt(4, worker.getSalary());
            statement.execute();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            statement.execute();
        }
    }
}