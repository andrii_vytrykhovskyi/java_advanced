package dao.impl;

import dao.ProjectDAO;
import model.Project;
import util.SQLConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectDAOImpl implements ProjectDAO {
    private static final String GET = "select * from project";
    private static final String GET_ID = "select * from project where id = ?";
    private static final String GET_NAME = "select * from project where name = ?";
    private static final String INSERT = "insert into project (id, name, password, budget, role) values (?, ?, ?, ?,?)";
    private static final String DELETE = "delete from project where id=?";
    private static final String UPDATE = "update project set id = ?, name = ?, password = ?, budget = ?, role = ? where (id = ?)";


    private Connection sqlConnector;

    public ProjectDAOImpl() {
        sqlConnector = SQLConnector.getConnection();
    }

    @Override
    public List<Project> getAll() throws SQLException {
        List<Project> projects = new ArrayList<>();
        try (Statement statement = sqlConnector.createStatement();
             ResultSet resultSet = statement.executeQuery(GET)) {
            while (resultSet.next()) {
                Project project = new Project(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("password"), resultSet.getInt("budget"), resultSet.getString("role"));
                projects.add(project);
            }
        }
        return projects;
    }

    @Override
    public Project getById(int id) throws SQLException {
        Project project = null;
        try (PreparedStatement statement = sqlConnector.prepareStatement(GET_ID)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    project = new Project(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("password"), resultSet.getInt("budget"), resultSet.getString("role"));
                }
            }
        }
        return project;
    }

    @Override
    public Project getByName(String name) throws SQLException {
        Project project = null;
        try (PreparedStatement statement = sqlConnector.prepareStatement(GET_NAME)) {
            statement.setString(1, name);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    project = new Project(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("password"), resultSet.getInt("budget"), resultSet.getString("role"));
                }
            }
        }
        return project;
    }

    @Override
    public void create(Project project) throws SQLException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(INSERT)) {
            statement.setInt(1, project.getId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getPassword());
            statement.setInt(4, project.getBudget());
            statement.setString(5, project.getRole());
            statement.execute();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            statement.execute();
        }
    }

    @Override
    public void update(int id, Project project) throws SQLException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(UPDATE)) {
            statement.setInt(1, project.getId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getPassword());
            statement.setInt(4, project.getBudget());
            statement.setString(5, project.getRole());
            statement.setInt(6, id);
            statement.execute();
        }
    }
}