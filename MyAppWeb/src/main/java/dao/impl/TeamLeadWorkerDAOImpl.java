package dao.impl;

import dao.TeamLeadWorkerDAO;
import exception.NotFoundIdException;
import model.TeamLeadWorker;
import util.SQLConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamLeadWorkerDAOImpl implements TeamLeadWorkerDAO {
    private static final String GET = "select * from team_lead_worker";
    private static final String INSERT = "insert into team_lead_worker (team_lead_id, worker_id) values (?, ?)";
    private static final String DELETE = "delete from team_lead_worker where team_lead_id=? and worker_id=?";

    private Connection sqlConnector;

    public TeamLeadWorkerDAOImpl() {
        sqlConnector = SQLConnector.getConnection();
    }


    @Override
    public List<TeamLeadWorker> readAll() throws SQLException {
        List<TeamLeadWorker> teamLeadWorkers = new ArrayList<>();
        try (Statement statement = sqlConnector.createStatement();
             ResultSet resultSet = statement.executeQuery(GET)) {
            while (resultSet.next()) {
                TeamLeadWorker teamLeadWorker = new TeamLeadWorker(resultSet.getInt("team_lead_id"), resultSet.getInt("worker_id"));
                teamLeadWorkers.add(teamLeadWorker);
            }
        }
        return teamLeadWorkers;
    }

    @Override
    public void create(TeamLeadWorker teamLeadWorker) {
        executeQuery(INSERT, teamLeadWorker);
    }

    @Override
    public void delete(int teamLeadId, int workerId) throws SQLException, NotFoundIdException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(DELETE)) {
            statement.setInt(1, teamLeadId);
            statement.setInt(2, workerId);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void executeQuery(String query, TeamLeadWorker teamLeadWorker) {
        try (PreparedStatement statement = sqlConnector.prepareStatement(query)) {
            statement.setInt(1, teamLeadWorker.getTeamLeadId());
            statement.setInt(2, teamLeadWorker.getWorkerId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}