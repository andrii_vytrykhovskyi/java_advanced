package dao.impl;

import dao.WorkerModelDAO;
import model.WorkerModel;
import util.SQLConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WorkerModelDAOImpl implements WorkerModelDAO {

    private static final String GET_WORKERS_FOR_TEAMLEAD = "SELECT worker.id, worker.full_name, worker.title, worker.salary " +
            "FROM worker INNER JOIN team_lead_worker ON worker.id =team_lead_worker.team_lead_id " +
            "WHERE team_lead_worker.worker_id =?";

    private Connection sqlConnector;

    public WorkerModelDAOImpl() {
        sqlConnector = SQLConnector.getConnection();
    }

    @Override
    public List<WorkerModel> getWorkersByTeamLead(int teamLeadId) throws SQLException {
        List<WorkerModel> workers = new ArrayList<>();
        ResultSet resultSet = null;
        try (PreparedStatement statement = sqlConnector.prepareStatement(GET_WORKERS_FOR_TEAMLEAD)) {
            statement.setInt(1, teamLeadId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                WorkerModel worker = new WorkerModel(resultSet.getInt("id"), resultSet.getString("full_name"),
                        resultSet.getString("title"),
                        resultSet.getInt("salary"));
                workers.add(worker);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            resultSet.close();
        }
        return workers;
    }
}

