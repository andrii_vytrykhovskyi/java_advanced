package dao.impl;

import dao.TeamLeadDAO;
import model.TeamLead;
import util.SQLConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamLeadDAOImpl implements TeamLeadDAO {
    private static final String GET = "select * from team_lead";
    private static final String GET_ID = "select * from team_lead where id = ?";
    private static final String INSERT = "insert into team_lead (id, full_name, date_of_birth, salary) values (?, ?, ?, ?)";
    private static final String DELETE = "delete from team_lead where id=?";

    private Connection sqlConnector;

    public TeamLeadDAOImpl() {
        sqlConnector = SQLConnector.getConnection();
    }

    @Override
    public List<TeamLead> getAll() throws SQLException {
        List<TeamLead> teamLeads = new ArrayList<>();
        try (Statement statement = sqlConnector.createStatement();
             ResultSet resultSet = statement.executeQuery(GET)) {
            while (resultSet.next()) {
                TeamLead teamLead = new TeamLead(resultSet.getInt("id"), resultSet.getString("full_name"), resultSet.getDate("date_of_birth"), resultSet.getInt("salary"));
                teamLeads.add(teamLead);
            }
        }
        return teamLeads;
    }

    @Override
    public TeamLead getById(int id) throws SQLException {
        TeamLead teamLead = null;
        try (PreparedStatement statement = sqlConnector.prepareStatement(GET_ID)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    teamLead = new TeamLead(resultSet.getInt("id"), resultSet.getString("full_name"), resultSet.getDate("date_of_birth"), resultSet.getInt("salary"));
                }
            }
        }
        return teamLead;
    }

    @Override
    public void create(TeamLead teamLead) throws SQLException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(INSERT)) {
            statement.setInt(1, teamLead.getId());
            statement.setString(2, teamLead.getFullName());
            statement.setDate(3, (Date) teamLead.getDateOfBirth());
            statement.setInt(4, teamLead.getSalary());
            statement.execute();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement statement = sqlConnector.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            statement.execute();
        }
    }
}