package dao;

import model.WorkerModel;

import java.sql.SQLException;
import java.util.List;

public interface WorkerModelDAO {

    List<WorkerModel> getWorkersByTeamLead(int teamLeadId) throws SQLException;
}
