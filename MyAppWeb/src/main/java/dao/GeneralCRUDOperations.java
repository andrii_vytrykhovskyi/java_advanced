package dao;

import exception.DuplicateIdException;
import exception.NotFoundIdException;

import java.sql.SQLException;
import java.util.List;

public interface GeneralCRUDOperations<T> {
    List<T> getAll() throws SQLException;

    T getById(int id) throws SQLException, NotFoundIdException;

    void create(T t) throws SQLException, DuplicateIdException;

    void delete(int id) throws SQLException, NotFoundIdException;
}