package dao;

import exception.DuplicateIdException;
import exception.NotFoundIdException;
import model.Worker;

import java.sql.SQLException;

public interface WorkerDAO extends GeneralCRUDOperations<Worker> {
    void update(int id, Worker worker) throws SQLException, DuplicateIdException, NotFoundIdException;
}