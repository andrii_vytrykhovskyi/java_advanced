package dao;

import model.TeamLead;

public interface TeamLeadDAO extends GeneralCRUDOperations<TeamLead> {
}