package dao;

import exception.DuplicateIdException;
import exception.NotFoundIdException;
import model.Project;

import java.sql.SQLException;

public interface ProjectDAO extends GeneralCRUDOperations<Project> {
    void update(int id, Project project) throws SQLException, DuplicateIdException, NotFoundIdException;

    Project getByName(String name) throws SQLException;
}