package util;

public class RandomIntGenerator {

    private static final int min = 1;
    private static final int max = Integer.MAX_VALUE;


    public static int getRandomInt() {
        return (int) (Math.random() * ((max - min) + 1)) + min;
    }
}
