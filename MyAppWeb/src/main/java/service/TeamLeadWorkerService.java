package service;

import exception.DuplicateIdException;
import exception.NotFoundIdException;
import model.TeamLeadWorker;

import java.sql.SQLException;
import java.util.List;

public interface TeamLeadWorkerService {
    List<TeamLeadWorker> readAll() throws SQLException;

    void create(TeamLeadWorker teamLeadWorker) throws SQLException, DuplicateIdException;

    void delete(int teamLeadId, int workerId) throws SQLException, NotFoundIdException;
}
