package service;

import dao.GeneralCRUDOperations;
import dao.ProjectDAO;
import model.Project;

public interface ProjectService extends GeneralCRUDOperations<Project>, ProjectDAO {
}