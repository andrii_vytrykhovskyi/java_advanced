package service;

import dao.GeneralCRUDOperations;
import model.TeamLead;

public interface TeamLeadService extends GeneralCRUDOperations<TeamLead> {
}