package service.impl;

import dao.TeamLeadWorkerDAO;
import dao.impl.TeamLeadWorkerDAOImpl;
import exception.DuplicateIdException;
import exception.NotFoundIdException;
import model.TeamLeadWorker;
import org.apache.log4j.Logger;
import service.TeamLeadWorkerService;

import java.sql.SQLException;
import java.util.List;

public class TeamLeadWorkerServiceImpl implements TeamLeadWorkerService {

    private TeamLeadWorkerDAO teamLeadWorkerDAO;
    private static final Logger logger = Logger.getLogger(TeamLeadWorkerServiceImpl.class);

    public TeamLeadWorkerServiceImpl() {
        teamLeadWorkerDAO = new TeamLeadWorkerDAOImpl();
    }

    @Override
    public List<TeamLeadWorker> readAll() throws SQLException {
        logger.info("Read all team_lead-worker (s) request");
        return teamLeadWorkerDAO.readAll();
    }

    @Override
    public void create(TeamLeadWorker teamLeadWorker) throws SQLException, DuplicateIdException {
        if (isExists(teamLeadWorker.getTeamLeadId(), teamLeadWorker.getWorkerId())) {
            logger.error("TeamLeadWorker with team_lead_id: " + teamLeadWorker.getTeamLeadId() +
                    " and worker_id: " + teamLeadWorker.getWorkerId() + " already exists and can`t be created");
            throw new DuplicateIdException("TeamLeadWorker with team_lead_id: " + teamLeadWorker.getTeamLeadId()
                    + " and worker_id: " + teamLeadWorker.getWorkerId() + " already exists");
        } else {
            logger.info("Creating TeamLeadWorker : " + teamLeadWorker);
            teamLeadWorkerDAO.create(teamLeadWorker);
        }
    }

    @Override
    public void delete(int teamLeadId, int workerId) throws SQLException, NotFoundIdException {
        if (!isExists(teamLeadId, workerId)) {
            logger.error("TeamLeadWorker with team_lead_id: " + teamLeadId +
                    " and worker_id: " + workerId + " not found and can`t be deleted");
            throw new NotFoundIdException("TeamLeadWorker with team_lead_id: " + teamLeadId +
                    " and worker_id: " + workerId + " not found");
        } else {
            teamLeadWorkerDAO.delete(teamLeadId, workerId);
            logger.info("TeamLeadWorker with team_lead_id: " + teamLeadId +
                    " and worker_id: " + workerId + " was deleted");
        }
    }

    private List<TeamLeadWorker> getAll() throws SQLException {
        return teamLeadWorkerDAO.readAll();
    }

    private boolean isExists(int teamLeadId, int workerId) throws SQLException {
        boolean flag = false;
        for (TeamLeadWorker entity : getAll()) {
            if (teamLeadId == entity.getTeamLeadId() && workerId == entity.getWorkerId()) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}
