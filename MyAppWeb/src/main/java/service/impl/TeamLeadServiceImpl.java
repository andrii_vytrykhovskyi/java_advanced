package service.impl;

import dao.impl.TeamLeadDAOImpl;
import exception.DuplicateIdException;
import exception.NotFoundIdException;
import model.TeamLead;
import org.apache.log4j.Logger;
import service.TeamLeadService;

import java.sql.SQLException;
import java.util.List;

public class TeamLeadServiceImpl implements TeamLeadService {

    private TeamLeadDAOImpl teamLeadDAO;
    private static final Logger logger = Logger.getLogger(TeamLeadServiceImpl.class);

    public TeamLeadServiceImpl() {
        teamLeadDAO = new TeamLeadDAOImpl();
    }

    @Override
    public List<TeamLead> getAll() throws SQLException {
        logger.info("Get all team leads request");
        return teamLeadDAO.getAll();
    }

    @Override
    public TeamLead getById(int id) throws SQLException, NotFoundIdException {
        TeamLead teamLead = teamLeadDAO.getById(id);
        if (teamLead != null) {
            logger.info("Getting team leads with id " + id);
            logger.info(teamLead);
            return teamLead;
        }
        logger.error("There is no team lead with id " + id);
        return null;
    }

    @Override
    public void create(TeamLead teamLead) throws SQLException, DuplicateIdException {
        if (isExist(teamLead.getId())) {
            logger.error("Team lead with id " + teamLead.getId() + " is already exists and can`t be created");
            throw new DuplicateIdException("Team lead with id " + teamLead.getId() + " is already exists");
        } else {
            logger.info("Creating team lead : " + teamLead);
            teamLeadDAO.create(teamLead);
        }
    }

    @Override
    public void delete(int id) throws SQLException, NotFoundIdException {
        if (isExist(id)) {
            teamLeadDAO.delete(id);
            logger.info("Team lead with id " + id + " was deleted");
        } else {
            logger.error("Team lead with id " + id + " not found and can`t be deleted");
            throw new NotFoundIdException("There is no team lead with id " + id);
        }
    }

    private boolean isExist(int id) throws SQLException {
        boolean isDuplicate = false;
        for (TeamLead teamLead : getAllRecords()) {
            if (teamLead.getId() == id) {
                isDuplicate = true;
                break;
            }
        }
        return isDuplicate;
    }

    private List<TeamLead> getAllRecords() throws SQLException {
        return teamLeadDAO.getAll();
    }
}