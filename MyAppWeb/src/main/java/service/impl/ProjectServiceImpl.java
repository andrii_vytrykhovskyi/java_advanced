package service.impl;

import dao.impl.ProjectDAOImpl;
import exception.DuplicateIdException;
import exception.NotFoundIdException;
import model.Project;
import org.apache.log4j.Logger;
import service.ProjectService;

import java.sql.SQLException;
import java.util.List;

public class ProjectServiceImpl implements ProjectService {

    private ProjectDAOImpl projectDAO;
    private static final Logger logger = Logger.getLogger(ProjectServiceImpl.class);

    public ProjectServiceImpl() {
        projectDAO = new ProjectDAOImpl();
    }

    @Override
    public void update(int id, Project project) throws SQLException, DuplicateIdException, NotFoundIdException {
        if (isExist(id)) {
            if (!isExist(project.getId())) {
                projectDAO.update(id, project);
                logger.info("Project with id " + id + " was updated by user " + project);
            } else {
                logger.error("Project with id " + project.getId() + " already exists. Please choose other id to update");
                throw new DuplicateIdException("Project with id " + project.getId() + " is already exists");
            }
        } else {
            logger.error("Project with id " + id + " not found and can`t be updated");
            throw new NotFoundIdException("There is no project with id " + id);
        }
    }

    @Override
    public Project getByName(String name) throws SQLException {
        return projectDAO.getByName(name);

    }

    @Override
    public List<Project> getAll() throws SQLException {
        logger.info("Get all projects request");
        return projectDAO.getAll();
    }

    @Override
    public Project getById(int id) throws SQLException, NotFoundIdException {
        Project project = projectDAO.getById(id);
        if (project == null) {
            logger.error("There is no project with id " + id);
            throw new NotFoundIdException("There is no project with id " + id);
        } else {
            logger.info("Getting project with id " + id);
            logger.info(project);
            return project;
        }
    }

    @Override
    public void create(Project project) throws SQLException, DuplicateIdException {
        if (isExist(project.getId())) {
            logger.error("Project with id " + project.getId() + " is already exists and can`t be created");
            throw new DuplicateIdException("Project with id " + project.getId() + " is already exists");
        } else {
            logger.info("Creating project : " + project);
            projectDAO.create(project);
        }
    }

    @Override
    public void delete(int id) throws SQLException, NotFoundIdException {
        if (isExist(id)) {
            projectDAO.delete(id);
            logger.info("Project with id " + id + " was deleted");
        } else {
            logger.error("Project with id " + id + " not found and can`t be deleted");
            throw new NotFoundIdException("There is no project with id " + id);
        }
    }

    private boolean isExist(int id) throws SQLException {
        boolean isDuplicate = false;
        for (Project project : getAllRecords()) {
            if (project.getId() == id) {
                isDuplicate = true;
                break;
            }
        }
        return isDuplicate;
    }

    private List<Project> getAllRecords() throws SQLException {
        return projectDAO.getAll();
    }
}