package service.impl;

import dao.WorkerModelDAO;
import dao.impl.WorkerModelDAOImpl;
import model.WorkerModel;
import org.apache.log4j.Logger;
import service.WorkerModelService;

import java.sql.SQLException;
import java.util.List;

public class WorkerModelServiceImpl implements WorkerModelService {

    private WorkerModelDAO workerModelDAO;
    private static final Logger logger = Logger.getLogger(WorkerModelServiceImpl.class);

    public WorkerModelServiceImpl() {
        workerModelDAO = new WorkerModelDAOImpl();
    }

    @Override
    public List<WorkerModel> getWorkerByTeamLead(int teamLeadId) throws SQLException {
        logger.info("Read all workers for teamlead request");
        return workerModelDAO.getWorkersByTeamLead(teamLeadId);
    }
}
