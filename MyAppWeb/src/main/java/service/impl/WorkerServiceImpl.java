package service.impl;

import dao.WorkerDAO;
import dao.impl.WorkerDAOImpl;
import exception.DuplicateIdException;
import exception.NotFoundIdException;
import model.Worker;
import org.apache.log4j.Logger;
import service.WorkerService;

import java.sql.SQLException;
import java.util.List;

public class WorkerServiceImpl implements WorkerService {

    private WorkerDAO workerDAO;
    private static final Logger logger = Logger.getLogger(WorkerServiceImpl.class);

    public WorkerServiceImpl() {
        workerDAO = new WorkerDAOImpl();
    }

    @Override
    public void update(int id, Worker worker) throws SQLException, DuplicateIdException, NotFoundIdException {
        if (isExist(id)) {
            workerDAO.update(id, worker);
            logger.info("Worker with id " + id + " was updated by worker: " + worker);
        } else {
            logger.error("Worker with id " + id + " not found and can`t be updated");
            throw new NotFoundIdException("There is no worker with id " + id);
        }
    }

    @Override
    public List<Worker> getAll() throws SQLException {
        logger.info("Read all workers request");
        return workerDAO.getAll();
    }

    @Override
    public Worker getById(int id) throws SQLException, NotFoundIdException {
        Worker worker = workerDAO.getById(id);
        if (worker == null) {
            logger.error("There is no worker with id " + id);
            throw new NotFoundIdException("There is no worker with id " + id);
        } else {
            logger.info("Getting worker with id " + id);
            logger.info(worker);
            return worker;
        }
    }

    @Override
    public void create(Worker worker) throws SQLException, DuplicateIdException {
        if (isExist(worker.getId())) {
            logger.error("Worker with id " + worker.getId() + " is already exists and can`t be created");
            throw new DuplicateIdException("Worker with id " + worker.getId() + " is already exists");
        } else {
            logger.info("Creating worker : " + worker);
            workerDAO.create(worker);
        }
    }

    @Override
    public void delete(int id) throws SQLException, NotFoundIdException {
        if (isExist(id)) {
            workerDAO.delete(id);
            logger.info("Worker with id " + id + " was deleted");
        } else {
            logger.error("Worker with id " + id + " not found and can`t be deleted");
            throw new NotFoundIdException("There is no worker with id " + id);
        }
    }

    private boolean isExist(int id) throws SQLException {
        boolean isDuplicate = false;
        for (Worker worker : getAllRecords()) {
            if (worker.getId() == id) {
                isDuplicate = true;
                break;
            }
        }
        return isDuplicate;
    }

    private List<Worker> getAllRecords() throws SQLException {
        return workerDAO.getAll();
    }
}