package service;

import dao.GeneralCRUDOperations;
import dao.WorkerDAO;
import model.Worker;

public interface WorkerService extends GeneralCRUDOperations<Worker>, WorkerDAO {
}