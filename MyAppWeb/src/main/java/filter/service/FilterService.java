package filter.service;


import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class FilterService {

    public void doFilterValidation(ServletRequest request, ServletResponse response, FilterChain chain,
                                   List<String> allowedRoles) throws ServletException, IOException {
        try {
            HttpSession session = ((HttpServletRequest) request).getSession();
            String role = session.getAttribute("role").toString();

            if (!role.isEmpty() && allowedRoles.contains(role)) {
                chain.doFilter(request, response);
            } else {
                ((HttpServletRequest) request).getRequestDispatcher("index.jsp").forward(request, response);
            }
        } catch (Exception exc) {
            ((HttpServletRequest) request).getRequestDispatcher("index.jsp").forward(request, response);
        }
    }
}
