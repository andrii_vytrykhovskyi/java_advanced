package servlet;

import lombok.SneakyThrows;
import model.Project;
import org.apache.log4j.Logger;
import service.ProjectService;
import service.impl.ProjectServiceImpl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;


public class RegistrationServlet extends HttpServlet {
    private ProjectService projectService;
    private static final Logger logger = Logger.getLogger(RegistrationServlet.class);

    public RegistrationServlet() {
        projectService = new ProjectServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String name = req.getParameter("projectName");
        if (Objects.isNull(projectService.getByName(name))) {
            logger.info("Registration new project " + name);
            String password = req.getParameter("projectPassword");
            int budget = Integer.parseInt(req.getParameter("projectBudget"));

            if (!name.isEmpty() && !password.isEmpty() && budget >= 0) {
                Project project = new Project(name, password, budget, "user");
                projectService.create(project);
                logger.info("Project was registered : " + project);

                HttpSession httpSession = req.getSession(true);
                httpSession.setAttribute("projectName", name);
            }
            resp.setContentType("text/plain");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write("Success");
        } else {
            logger.info("Project with name " + name + " already registered! Redirection to login page...");
            resp.setContentType("text/plain");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write("To login");
        }
    }
}