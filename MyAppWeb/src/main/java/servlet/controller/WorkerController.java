package servlet.controller;

import lombok.SneakyThrows;
import model.Worker;
import org.apache.log4j.Logger;
import service.WorkerService;
import service.impl.WorkerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/worker")
public class WorkerController extends HttpServlet {

    private final WorkerService workerService;
    private static final Logger logger = Logger.getLogger(WorkerController.class);

    public WorkerController() {
        workerService = new WorkerServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String name = req.getParameter("workerName");
        String title = req.getParameter("workerTitle");
        int salary = Integer.parseInt(req.getParameter("workerSalary"));

        Worker worker = new Worker(name, title, salary);
        logger.info("POST /worker request: " + worker);
        workerService.create(worker);
        //TODO : create handler (bootstrap component) if product is created
        resp.setContentType("text");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");
    }

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        logger.info("GET /worker by id : " + id);

        Worker worker = workerService.getById(id);
        logger.info("Worker: " + worker);

        req.setAttribute("worker", worker);
        req.getRequestDispatcher("singleWorker.jsp").forward(req, resp);
    }
}

