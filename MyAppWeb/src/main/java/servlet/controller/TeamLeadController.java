package servlet.controller;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import model.Project;
import model.TeamLead;
import model.TeamLeadWorker;
import model.WorkerModel;
import org.apache.log4j.Logger;
import service.ProjectService;
import service.TeamLeadService;
import service.TeamLeadWorkerService;
import service.WorkerModelService;
import service.impl.ProjectServiceImpl;
import service.impl.TeamLeadServiceImpl;
import service.impl.TeamLeadWorkerServiceImpl;
import service.impl.WorkerModelServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

@WebServlet("/teamLead")
public class TeamLeadController extends HttpServlet {

    private static final Logger logger = Logger.getLogger(TeamLeadController.class);
    private TeamLeadWorkerService teamLeadWorkerService;
    private TeamLeadService teamLeadService;
    private ProjectService projectService;
    private WorkerModelService workerModelService;

    public TeamLeadController() {
        teamLeadWorkerService = new TeamLeadWorkerServiceImpl();
        teamLeadService = new TeamLeadServiceImpl();
        projectService = new ProjectServiceImpl();
        workerModelService = new WorkerModelServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("GET request : get workers by teamLead");
        int teamLeadId = getProjectAndTeamLeadIdParam(req);
        List<WorkerModel> workers = workerModelService.getWorkerByTeamLead(teamLeadId);
        logger.info("Workers: " + workers);
        String json = new Gson().toJson(workers);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(json);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("POST request: add worker to Team Lead");
        int workerId = Integer.parseInt(req.getParameter("workerId"));
        int teamLeadId = getProjectAndTeamLeadIdParam(req);

        if (Objects.isNull(teamLeadService.getById(teamLeadId))) {
            TeamLead teamLead = new TeamLead(teamLeadId);
            logger.info("POST request: new Team Lead was added " + teamLead);
        }


        TeamLeadWorker teamLeadWorker = new TeamLeadWorker(teamLeadId, workerId);
        logger.info("Worker: " + workerId + ", was added to Team Lead " + teamLeadId);
        teamLeadWorkerService.create(teamLeadWorker);

        resp.setContentType("text");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");
    }

    @SneakyThrows
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("DELETE request: delete worker from team lead");

        int workerId = Integer.parseInt(req.getParameter("workerId"));
        int teamLeadId = getProjectAndTeamLeadIdParam(req);
        teamLeadWorkerService.delete(teamLeadId, workerId);
        logger.info("Deleted worker: " + workerId + ", from team lead: " + teamLeadId);

        resp.setContentType("text");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");
    }

    @SneakyThrows
    private int getProjectAndTeamLeadIdParam(HttpServletRequest req) {
        HttpSession session = req.getSession();
        String projectName = session.getAttribute("projectName").toString();
        Project project = projectService.getByName(projectName);
        return project.getId();
    }
}
