package servlet;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import model.Project;
import model.ProjectLogin;
import org.apache.log4j.Logger;
import service.ProjectService;
import service.impl.ProjectServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private ProjectService projectService;
    private static final Logger logger = Logger.getLogger(LoginServlet.class);

    public LoginServlet() {
        projectService = new ProjectServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");

        Project project = projectService.getByName(name);
        if (!Objects.isNull(project)) {
            String password = req.getParameter("password");
            if (project.getPassword().equals(password)) {
                logger.info("Project with name: " + name + " was logged in");
                ProjectLogin projectLogin = new ProjectLogin(name, "cabinet.jsp");

                HttpSession httpSession = req.getSession(true);
                httpSession.setAttribute("projectName", name);
                httpSession.setAttribute("role", project.getRole());

                String json = new Gson().toJson(projectLogin);
                logger.info("JSON login: " + json);
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write(json);
            } else {

                logger.info("Wrong password for project with email: " + name);
                resp.setContentType("text");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write("Wrong password");
            }
        } else {
            logger.info("Project with email : " + name + " is not registered. Redirection to registration page.");

            resp.setContentType("text");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write("User is NOT registered");
        }
    }
}