package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import util.RandomIntGenerator;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {
    private int id;
    private String name;
    private String password;
    private int budget;
    private String role;

    public Project(String name, String password, int budget, String role) {
        this.id = RandomIntGenerator.getRandomInt();
        this.name = name;
        this.password = password;
        this.budget = budget;
        this.role = role;
    }
}

