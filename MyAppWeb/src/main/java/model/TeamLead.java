package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import util.RandomIntGenerator;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeamLead {
    private int id;
    private String fullName;
    private Date dateOfBirth;
    private int salary;

    public TeamLead(String fullName, Date dateOfBirth, int salary) {
        this.id = RandomIntGenerator.getRandomInt();
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.salary = salary;
    }

    public TeamLead(int id) {
        this.id = id;
    }
}