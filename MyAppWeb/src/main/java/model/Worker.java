package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import util.RandomIntGenerator;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Worker {
    private int id;
    private String fullName;
    private String title;
    private int salary;

    public Worker(String fullName, String title, int salary) {
        this.id = RandomIntGenerator.getRandomInt();
        this.fullName = fullName;
        this.title = title;
        this.salary = salary;
    }
}