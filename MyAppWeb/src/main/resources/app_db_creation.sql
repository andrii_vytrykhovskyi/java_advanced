CREATE SCHEMA IF NOT EXISTS `app_db`;
USE `app_db`;

-- -----------------------------------------------------
-- Table `app_db`.`project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `app_db`.`project`
(
    `id`       INT(11)     NOT NULL PRIMARY KEY,
    `name`     VARCHAR(50) NOT NULL,
    `password` VARCHAR(45) NULL DEFAULT NULL,
    `budget`   INT(11)     NOT NULL

);

-- -----------------------------------------------------
-- Table `app_db`.`team_lead`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `app_db`.`team_lead`
(
    `id`            INT(11)     NOT NULL PRIMARY KEY,
    `full_name`     VARCHAR(50) NOT NULL,
    `date_of_birth` DATE        NULL DEFAULT NULL,
    `salary`        INT(11)     NOT NULL,
    FOREIGN KEY (`id`)
        REFERENCES `app_db`.`project` (`id`)
);

-- -----------------------------------------------------
-- Table `app_db`.`worker`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `app_db`.`worker`
(
    `id`        INT(11)     NOT NULL PRIMARY KEY,
    `full_name` VARCHAR(50) NOT NULL,
    `title`     VARCHAR(50) NOT NULL,
    `salary`    INT(11)     NOT NULL
);


CREATE TABLE IF NOT EXISTS `app_db`.`team_lead_worker`
(
    `team_lead_id` INT(11) NOT NULL,
    `worker_id`    INT(11) NOT NULL,
    CONSTRAINT `id` PRIMARY KEY (`team_lead_id`, `worker_id`),
    FOREIGN KEY (`team_lead_id`) REFERENCES `app_db`.`team_lead` (`id`),
    FOREIGN KEY (`worker_id`) REFERENCES `app_db`.`worker` (`id`)
);

ALTER TABLE project
    ADD COLUMN role VARCHAR(10);