package exception;

public class UserIsNotAdultException extends Exception {
    public UserIsNotAdultException(String message) {
        super(message);
    }
}
