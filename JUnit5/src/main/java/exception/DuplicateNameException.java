package exception;

public class DuplicateNameException extends Throwable {
    public DuplicateNameException(String message) {
        super(message);
    }
}
