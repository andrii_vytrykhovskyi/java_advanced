package service;

import dao.Animal;
import dao.User;
import exception.DuplicateNameException;
import exception.UserIsNotAdultException;

public interface UserService {

    boolean isUserAdult(User user) throws UserIsNotAdultException;

    void addAnimalToUser(Animal animal, User user) throws DuplicateNameException;

    void deleteAnimalFromUser(Animal animal, User user);

    void deleteAddress(User user);

    void changeName(User user, String newName);
}
