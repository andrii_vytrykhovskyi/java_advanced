package service;

import dao.Animal;
import dao.User;
import exception.DuplicateNameException;
import exception.UserIsNotAdultException;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserServiceImpl implements UserService {

    @Override
    public boolean isUserAdult(User user) throws UserIsNotAdultException {
        if (user.getAge() < 18) throw new UserIsNotAdultException("User is not adult");
        else return true;
    }

    @Override
    public void addAnimalToUser(Animal animal, User user) throws DuplicateNameException {
        if (!duplicatedAnimal(animal.getName(), user)) {
            user.getAnimals().add(animal);
        } else {
            throw new DuplicateNameException("You already have an animal with name " + animal.getName());
        }
    }

    private boolean duplicatedAnimal(String name, User user) {
        for (Animal animal : user.getAnimals())
            if (animal.getName().equals(name))
                return true;
        return false;
    }

    @Override
    public void deleteAnimalFromUser(Animal animal, User user) {
        user.getAnimals().removeIf(x -> (x.getAnimalType().equals(animal.getAnimalType())) && x.getName().equals(animal.getName()));
    }

    @Override
    public void deleteAddress(User user) {
        user.setAddress(null);
    }

    @Override
    public void changeName(User user, String newName) {
        user.setFullName(newName);
    }
}


