import dao.Address;
import dao.Animal;
import dao.AnimalType;
import dao.User;
import exception.DuplicateNameException;
import exception.UserIsNotAdultException;
import org.junit.jupiter.api.*;
import service.UserService;
import service.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;

class UserServiceImplTest {

    private static UserService service;
    private static User user;

    @BeforeEach
    void createUser() {
        user = createTestUser();
    }

    @BeforeAll
    static void init() {
        service = new UserServiceImpl();
    }

    @Test
    @DisplayName("Test isUserAdult -> success")
    void isUserAdultTest() throws UserIsNotAdultException {
        Assertions.assertTrue(service.isUserAdult(new User("Andrii", null, null, 30)));
    }

    @Test
    @DisplayName("Test isUserAdult -> with exception")
    void isUserAdultTestWithException() {
        Assertions.assertThrows(UserIsNotAdultException.class, () ->
                service.isUserAdult(new User("Andrii", null, null, 17)));
    }

    @Test
    @DisplayName("Test deleteAddress -> success")
    void deleteAddressTest() {
        service.deleteAddress(user);
        Address expected = user.getAddress();
        Assertions.assertNull(expected);
    }

    @Test
    @DisplayName("Test addAnimalToUser -> success")
    void addAnimalToUserTest() throws DuplicateNameException {
        Animal animalToAdd = new Animal(AnimalType.DOG, "Dog");
        service.addAnimalToUser(animalToAdd, user);

        List<Animal> actualAnimals = user.getAnimals();
        List<Animal> expectedAnimals = new ArrayList<>();

        expectedAnimals.add(new Animal(AnimalType.CAT, "Meow"));
        expectedAnimals.add(new Animal(AnimalType.DOG, "Gav"));
        expectedAnimals.add(new Animal(AnimalType.DOG, "Dog"));

        Assertions.assertEquals(expectedAnimals, actualAnimals);
    }

    @Test
    @DisplayName("Test addAnimalToUser -> with exception")
    void addAnimalToUserTestWithException() {
        Assertions.assertThrows(DuplicateNameException.class, () -> service.addAnimalToUser(new Animal(AnimalType.DOG,
                "Meow"), user));
    }

    @Test
    @DisplayName("Test deleteAnimalFromUser -> with success")
    void deleteAnimalFromUserTest() {
        service.deleteAnimalFromUser(new Animal(AnimalType.CAT, "Meow"), user);

        List<Animal> actualAnimals = user.getAnimals();
        List<Animal> expectedAnimals = new ArrayList<>();

        expectedAnimals.add(new Animal(AnimalType.DOG, "Gav"));

        Assertions.assertEquals(expectedAnimals, actualAnimals);
    }

    @Test
    @DisplayName("Test changeName -> with success")
    void changeNameTest(){
        service.changeName(user, "New Name");

        String actual = user.getFullName();
        String expected = "New Name";

        Assertions.assertEquals(expected, actual);

    }

    private static User createTestUser() {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal(AnimalType.CAT, "Meow"));
        animals.add(new Animal(AnimalType.DOG, "Gav"));

        Address address = new Address("Banderu", 11, 1);

        return new User("Andrii", address, animals, 25);
    }
}