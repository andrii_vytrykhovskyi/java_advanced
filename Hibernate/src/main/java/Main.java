import model.Actor;
import model.Customer;
import model.Film;
import model.Order;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.SessionFactoryUtil;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
       // testManyToMany();
        testOneToMany();

    }

    private static void testOneToMany() {
        Session session = SessionFactoryUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Customer c1 = new Customer("Andrii Vytrykhovskyi");
        Customer c2 = new Customer("Uliana Boichuk");

        Order o1 = new Order(152.10, "TV", c1);
        Order o2 = new Order(163.15, "Sofa", c1);
        Order o3 = new Order(12.15, "Toys", c1);
        Order o4 = new Order(1122.19, "PC", c2);
        Order o5 = new Order(15343.00, "Earphones", c2);
        Order o6 = new Order(1526.00, "CD Player", c2);

        Set<Order> orders1 = new HashSet<>(Arrays.asList(o1, o2, o3));
        c1.setOrders(orders1);

        Set<Order> orders2 = new HashSet<>(Arrays.asList(o4, o5, o6));
        c2.setOrders(orders2);

        session.save(c1);
        session.save(c2);

        transaction.commit();
        session.close();
    }

    private static void testManyToMany() {
        Session session = SessionFactoryUtil.getSession();

        Transaction transaction = session.beginTransaction();

        Film film1 = new Film("Avatar", 1995);

        Actor actor1 = new Actor("Leonardo Di Caprio",50);
        Actor actor2 = new Actor("Matthew Mcconahi",54);
        Actor actor3 = new Actor("Andrii Vytrykhovskyi:)",25);

        film1.setActors(new HashSet<>(Arrays.asList(actor1, actor2, actor3)));

        Film film2 = new Film("Pirates of Carribean Sea", 2000);

        Actor actor4 = new Actor("Johny Depp",40);
        Actor actor5 = new Actor("Orlando Blum",44);
        Actor actor6 = new Actor("Kira Naytli:)",35);

        film2.setActors(new HashSet<>(Arrays.asList(actor4, actor5, actor6)));

        session.persist(film1);
        session.persist(film2);

        transaction.commit();
        session.close();
    }
}
