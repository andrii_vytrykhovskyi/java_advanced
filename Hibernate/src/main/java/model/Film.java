package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Film {
    private int id;
    private String name;
    private int year;
    private Set<Actor> actors = new HashSet<>();

    public Film(String name, int year) {
        this.name = name;
        this.year = year;
    }
}
