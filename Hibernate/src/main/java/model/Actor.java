package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Actor {
    private int id;
    private String fullName;
    private int age;
    private Set<Film> films = new HashSet<>();

    public Actor(String fullName, int age) {
        this.fullName = fullName;
        this.age = age;
    }
}
