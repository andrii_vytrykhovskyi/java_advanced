package model;

import com.sun.org.apache.xpath.internal.operations.Or;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private int id;

    @Column(name = "customer_full_name")
    private String customerFullName;

    @Column(name = "total_price")
    private double totalPrice;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Order> orders;

    public Customer(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
        this.totalPrice = getTotalPrice(orders);
    }

    private double getTotalPrice(Set<Order> orders){
        return orders.stream().mapToDouble(Order::getPrice).sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id &&
                Double.compare(customer.totalPrice, totalPrice) == 0 &&
                Objects.equals(customerFullName, customer.customerFullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerFullName, totalPrice);
    }
}
