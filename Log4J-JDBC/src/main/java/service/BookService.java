package service;

import dao.Book;

public interface BookService extends GeneralService<Book> {
}
