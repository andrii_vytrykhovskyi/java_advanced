package service;

import dao.Author;

public interface AuthorService extends GeneralService<Author> {
}
