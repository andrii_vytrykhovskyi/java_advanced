package service.impl;

import dao.Author;
import exception.DuplicateEntityException;
import exception.NoEntityException;
import jdbc.Connector;
import org.apache.log4j.Logger;
import service.AuthorService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AuthorServiceImpl implements AuthorService {
    private static Connection connection = Connector.getConnection();
    private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class);

    private static final String GET = "select * from autor";
    private static final String GET_ID = "select * from autor where id = ?";
    private static final String INSERT = "insert into autor (id, name, surname) values (?, ?, ?)";
    private static final String UPDATE = "update autor set name=?, surname=? where id=?";
    private static final String DELETE = "delete from autor where id=?";

    @Override
    public List<Author> getAll() {
        List<Author> authors = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(GET)) {
            while (result.next()) {
                authors.add(new Author(result.getInt("id"), result.getString("name"),
                        result.getString("surname")));
            }
            logger.info("Get all authors request was processed");
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return authors;
    }

    @Override
    public Author getById(int id) {
        Author author = null;
        try (PreparedStatement statement = connection.prepareStatement(GET_ID)) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    logger.info("Getting author by id: " + id);
                    author = new Author(result.getInt("id"), result.getString("name"), result.getString("surname"));
                } else {
                    throw new NoEntityException("No author with id: " + id);
                }
            }
        } catch (SQLException | NoEntityException e) {
            logger.error(e.getMessage());
        }
        return author;
    }

    @Override
    public void create(Author author) throws DuplicateEntityException {
        if (isExist(author.getId())) {
            logger.error("Author with id " + author.getId() + " already exists!");
            throw new DuplicateEntityException("Author with id " + author.getId() + " already exists!");
        } else {
            try (PreparedStatement statement = connection.prepareStatement(INSERT)) {
                statement.setInt(1, author.getId());
                statement.setString(2, author.getName());
                statement.setString(3, author.getSurname());
                logger.info("Create author with id: " + author.getId());
                statement.execute();
            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
        }
    }

    @Override
    public void update(Author author) throws NoEntityException {
        if (isExist(author.getId())) {
            try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
                statement.setString(1, author.getName());
                statement.setString(2, author.getSurname());
                statement.setInt(3, author.getId());
                statement.execute();
                logger.info("Update author with id: " + author.getId());
            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
        } else {
            logger.error("No author with id: " + author.getId() + " to update");
            throw new NoEntityException("Author with id " + author.getId() + " is not exist to update");
        }
    }

    @Override
    public void delete(int id) throws NoEntityException {
        if (isExist(id)) {
            try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
                statement.setInt(1, id);
                statement.execute();
                logger.info("Delete author with id: " + id);
            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
        } else {
            logger.error("No author with id: " + id + " to delete");
            throw new NoEntityException("Author with id " + id + " is not exist to delete");
        }
    }

    @Override
    public boolean isExist(int id) {
        boolean isDuplicate = false;
        for (Author author : getAll()) {
            if (author.getId() == id) isDuplicate = true;
        }
        return isDuplicate;
    }
}