package service.impl;

import dao.Author;
import dao.Book;
import exception.DuplicateEntityException;
import exception.NoEntityException;
import jdbc.Connector;
import org.apache.log4j.Logger;
import service.AuthorService;
import service.BookService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class BookServiceImpl implements BookService {

    private static final Logger logger = Logger.getLogger(BookServiceImpl.class);
    private static Connection connection = Connector.getConnection();
    private static AuthorService authorService = new AuthorServiceImpl();

    private static final String GET = "select * from book";
    private static final String GET_ID = "select * from book where id = ?";
    private static final String INSERT = "insert into book (id, name, description, price, autor_id) values (?, ?, ?, ?, ?)";
    private static final String UPDATE = "update book set name=?, description=?, price =?, autor_id=?  where id=?";
    private static final String DELETE = "delete from book where id=?";


    @Override
    public List<Book> getAll() {
        List<Book> books = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(GET)) {
            while (result.next()) {
                Author author = authorService.getById(result.getInt("autor_id"));
                Book book = new Book(result.getInt("id"), result.getString("name"),
                        result.getString("description"), result.getInt("price"), author);
                books.add(book);
            }
            logger.info("Get all books request was processed");
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return books;
    }

    @Override
    public Book getById(int id) {
        Book book = null;
        try (PreparedStatement statement = connection.prepareStatement(GET_ID)) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    Author author = authorService.getById(result.getInt("autor_id"));
                    book = new Book(result.getInt("id"), result.getString("name"),
                            result.getString("description"), result.getInt("price"), author);
                    logger.info("Getting book by id: " + id);

                } else {
                    throw new NoEntityException("No book with id: " + id);
                }
            } catch (NoEntityException | SQLException e) {
                logger.error(e.getMessage());
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return book;
    }

    @Override
    public void create(Book book) throws DuplicateEntityException, NoEntityException {
        if (authorService.isExist(book.getAuthor().getId())) {
            if (isExist(book.getId())) {
                logger.error("Book is already exists!");
                throw new DuplicateEntityException("Book with id : " + book.getId() + " already exists!");
            } else {
                try (PreparedStatement statement = connection.prepareStatement(INSERT)) {
                    statement.setInt(1, book.getId());
                    statement.setString(2, book.getName());
                    statement.setString(3, book.getDescription());
                    statement.setInt(4, book.getPrice());
                    statement.setInt(5, book.getAuthor().getId());
                    statement.execute();
                    logger.info("Creating book with id : " + book.getId());
                } catch (SQLException e) {
                    logger.error(e.getMessage());
                }
            }
        } else {
            logger.error("Author is not exist!");
            throw new NoEntityException("Author is not exist!");
        }
    }

    @Override
    public void update(Book book) throws DuplicateEntityException, NoEntityException {
        if (isExist(book.getId())) {
            if (authorService.isExist(book.getAuthor().getId())) {
                try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
                    statement.setString(1, book.getName());
                    statement.setString(2, book.getDescription());
                    statement.setInt(3, book.getPrice());
                    statement.setInt(4, book.getAuthor().getId());
                    statement.setInt(5, book.getId());
                    statement.execute();
                    logger.info("Updating book with id: " + book.getId());
                } catch (SQLException e) {
                    logger.error(e.getMessage());
                }
            } else {
                logger.error("Author is not exist!");
                throw new DuplicateEntityException("Author is not exist to update book");
            }
        } else {
            logger.error("No book with id: " + book.getId() + " to update");
            throw new NoEntityException("Book with id " + book.getId() + " is not exist to update");
        }
    }

    @Override
    public void delete(int id) throws DuplicateEntityException {
        if (isExist(id)) {
            try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
                statement.setInt(1, id);
                statement.execute();
                logger.info("Delete book with id: " + id);
            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
        } else {
            logger.error("No book with id: " + id + " to delete");
            throw new DuplicateEntityException("Book with id " + id + " is not exist to delete");
        }
    }

    @Override
    public boolean isExist(int id) {
        boolean isDuplicate = false;
        for (Book book : getAll()) {
            if (book.getId() == id) isDuplicate = true;
        }
        return isDuplicate;
    }
}