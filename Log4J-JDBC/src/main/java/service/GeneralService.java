package service;

import exception.DuplicateEntityException;
import exception.NoEntityException;

import java.sql.SQLException;
import java.util.List;

public interface GeneralService<T> {
    List<T> getAll() throws SQLException, NoEntityException;

    T getById(int id) throws SQLException;

    void create(T entity) throws SQLException, DuplicateEntityException, NoEntityException;

    void update(T entity) throws SQLException, DuplicateEntityException, NoEntityException;

    void delete(int id) throws SQLException, DuplicateEntityException, NoEntityException;

    boolean isExist(int id);
}
